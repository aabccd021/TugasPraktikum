package animals;

import cages.Cage;

public class Hamster extends Animal {
    public Hamster(String name, int length) {
        super(name, length, "pet");
    }

    public void treat1() {
        makeAVoice("ngkkrit.. ngkkrrriiit");
    }

    public void treat2() {
        makeAVoice("trrr…. trrr...");
    }


    public String treatDescription() {
        return "1: See it gnawing 2: Order to run in the hamster wheel";
    }
}
