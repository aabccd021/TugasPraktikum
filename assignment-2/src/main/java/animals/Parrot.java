package animals;

import cages.Cage;

import java.util.Scanner;

public class Parrot extends Animal {
    public Parrot(String name, int length) {
        super(name, length, "pet");
    }

    public Scanner treatWithScan(int index, Scanner input) {
        switch (index) {
            //mending this.brush atau brush(this)?
            case 1:
                treat1();
                break;
            case 2:
                input = treat2(input);
                break;
            case 3:
                treat3();
                break;
            default:
                System.out.println("You do nothing...");

        }
        System.out.println("Back to the office!\n");
        return input;
    }

    public void treat1() {
        System.out.println("Parrot " + this.name + " flies!");
        makeAVoice("FLYYYY…..");
    }

    public Scanner treat2(Scanner input) {
        System.out.print("You say:");
        String say = input.nextLine();
        System.out.println(this.name + " says: " + say.toUpperCase());
        return input;
    }

    public void treat3() {
        System.out.println(this.name + " says: HM");
    }

    public String treatDescription() {
        return "1: Order to fly 2: Do conversation";
    }


}
