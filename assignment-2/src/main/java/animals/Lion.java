package animals;

import cages.Cage;

public class Lion extends Animal {
    public Lion(String name, int length) {
        super(name, length, "wild");
    }

    public void treat1() {
        System.out.println("Lion is hunting");
        makeAVoice("err...");
    }

    public void treat2() {
        System.out.println("Clean the lion's mane..");
        makeAVoice("Hauhhmm!");
    }

    public void treat3() {
        makeAVoice("HAUHHMM!");
    }

    public String treatDescription() {
        return "1: See it hunting 2: Brush the mane 3: Disturb it";
    }
}
