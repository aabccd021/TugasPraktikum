package animals;

import cages.Cage;

public class Cat extends Animal {
    public Cat(String name, int length) {
        super(name, length, "pet");
    }

    public void treat1() {
        //mending banyak sout atau 1 sout + \n
        System.out.println("Time to clean " + this.name + "'s fur");
        makeAVoice("Nyaaan...");
    }

    public void treat2() {
        makeAVoice("Purrr...");
    }

    public String treatDescription() {
        return "1: Brush the fur 2: Cuddle";
    }
}
