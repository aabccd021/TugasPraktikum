package animals;

import cages.Cage;

public class Eagle extends Animal {
    public Eagle(String name, int length) {
        super(name, length, "wild");
    }

    public void treat1() {
        makeAVoice("kwaakk...");
        System.out.println("You hurt!");
    }

    public String treatDescription() {
        return "1: Order to fly";
    }
}
