package animals;

import cages.Cage;
import java.util.Scanner;


public class Animal {
    //protected?
    protected String name;
    protected int length;
    protected String type;
    protected Cage cage;

    protected Animal(String name, int length, String type) {
        this.name = name;
        this.length = length;
        this.type = type;
        this.cage = new Cage(this);
    }

    protected void makeAVoice(String voice) {
        System.out.println(this.name + " makes a voice: " + voice);
    }

    public void treat(int index) {
        switch (index) {
            //mending this.brush atau brush(this)?
            case 1:
                treat1();
                break;
            case 2:
                treat2();
                break;
            case 3:
                treat3();
                break;
            default:
                System.out.println("You do nothing...");

        }
        System.out.println("Back to the office!\n");
    }

    public Scanner treatWithScan(int index, Scanner scanner) {
        return null;
    }

    public String getName() {
        return this.name;
    }

    public int getLength() {
        return this.length;
    }

    public String getType() {
        return this.type;
    }

    public Cage getCage() {
        return this.cage;
    }

    public void treat1() {
        System.out.println("You do nothing...");
    }

    public void treat2() {
        System.out.println("You do nothing...");
    }

    public void treat3() {
        System.out.println("You do nothing...");
    }

    public String treatDescription() {
        return "";
    }
}
