import animals.Animal;
import animals.Cat;
import animals.Eagle;
import animals.Hamster;
import animals.Lion;
import animals.Parrot;
import cages.Cage;
import cages.CageManager;
import java.util.ArrayList;
import java.util.Scanner;

public class Manager {

    static Cage[] catsCage;
    static Cage[] lionsCage;
    static Cage[] eaglesCage;
    static Cage[] parrotsCage;
    static Cage[] hamstersCage;

    static ArrayList<ArrayList<Cage>> catsCageArrange;
    static ArrayList<ArrayList<Cage>> lionsCageArrange;
    static ArrayList<ArrayList<Cage>> eaglesCageArrange;
    static ArrayList<ArrayList<Cage>> parrotsCageArrange;
    static ArrayList<ArrayList<Cage>> hamstersCageArrange;

    static Scanner input;

    /**
     * Main method.
     *
     * @param args user input
     */
    public static void main(String[] args) {
        input = new Scanner(System.in);
        System.out.println("Welcome to Javari Park!\n"
                + "Input the number of animals");
        //meding next line terus parse int atau next int terus enter?
        //assign inputted animal to cage
        catsCage = assignAnimals("cat");
        lionsCage = assignAnimals("lion");
        eaglesCage = assignAnimals("eagle");
        parrotsCage = assignAnimals("parrot");
        hamstersCage = assignAnimals("hamster");

        System.out.println("Animals have been successfully recorded!\n");

        System.out.println("\n=============================================\n");

        //arrange cage
        if (catsCage.length != 0) {
            catsCageArrange = CageManager.arrangeCage(catsCage);
            System.out.println("location: " + catsCage[0].getLocation());
            CageManager.printArray(catsCageArrange);
            catsCageArrange = CageManager.rearrangeCage(catsCageArrange);
            System.out.println("After rearrangement...");
            CageManager.printArray(catsCageArrange);
        }

        if (lionsCage.length != 0) {
            lionsCageArrange = CageManager.arrangeCage(lionsCage);
            System.out.println("location: " + lionsCage[0].getLocation());
            CageManager.printArray(lionsCageArrange);
            lionsCageArrange = CageManager.rearrangeCage(lionsCageArrange);
            System.out.println("After rearrangement...");
            CageManager.printArray(lionsCageArrange);
        }

        if (eaglesCage.length != 0) {
            eaglesCageArrange = CageManager.arrangeCage(eaglesCage);
            System.out.println("location: " + eaglesCage[0].getLocation());
            CageManager.printArray(eaglesCageArrange);
            eaglesCageArrange = CageManager.rearrangeCage(eaglesCageArrange);
            System.out.println("After rearrangement...");
            CageManager.printArray(eaglesCageArrange);
        }

        if (parrotsCage.length != 0) {
            parrotsCageArrange = CageManager.arrangeCage(parrotsCage);
            System.out.println("location: " + parrotsCage[0].getLocation());
            CageManager.printArray(parrotsCageArrange);
            parrotsCageArrange = CageManager.rearrangeCage(parrotsCageArrange);
            System.out.println("After rearrangement...");
            CageManager.printArray(parrotsCageArrange);
        }

        if (hamstersCage.length != 0) {
            hamstersCageArrange = CageManager.arrangeCage(hamstersCage);
            System.out.println("location: " + hamstersCage[0].getLocation());
            CageManager.printArray(hamstersCageArrange);
            hamstersCageArrange = CageManager.rearrangeCage(hamstersCageArrange);
            System.out.println("After rearrangement...");
            CageManager.printArray(hamstersCageArrange);
        }

        //print number of each animal
        System.out.println("NUMBER OF ANIMALS:");
        System.out.println("cat:" + catsCage.length);
        System.out.println("lion:" + lionsCage.length);
        System.out.println("parrot:" + parrotsCage.length);
        System.out.println("eagle:" + eaglesCage.length);
        System.out.println("hamster:" + hamstersCage.length);
        System.out.println("\n=============================================");

        //treat animal
        boolean exit = false;
        while (!exit) {
            boolean found = false;
            System.out.println("Which animal you want to visit?");
            System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
            int selected = input.nextInt();
            input.nextLine();
            switch (selected) {
                case 1:
                    treatAnimal("cat", catsCage);
                    break;
                case 2:
                    treatAnimal("eagle", eaglesCage);
                    break;
                case 3:
                    treatAnimal("hamster", hamstersCage);
                    break;
                case 4:
                    treatAnimal("parrot", parrotsCage);
                    break;
                case 5:
                    treatAnimal("lion", lionsCage);
                    break;
                case 99:
                    exit = true;
                    break;
                default:

            }
        }
    }

    /**
     * assign specied animal in param using standard input.
     * @param species of animal to be assigned
     * @return
     */
    public static Cage[] assignAnimals(String species) {
        System.out.print(species + ": ");

        int numberOfTheSpecies = Integer.valueOf(input.nextInt());
        input.nextLine();

        //initialize cage
        Cage[] cage = new Cage[numberOfTheSpecies];
        if (numberOfTheSpecies == 0) {
            return null;
        }

        System.out.println("Provide the information of " + species + "(s):");
        String theSpeciesString = input.nextLine();
        //bisa dibikin method atau iterasi?
        String[] speciesSpecification = theSpeciesString.split(",");

        for (int i = 0; i < numberOfTheSpecies; i++) {

            String name = speciesSpecification[i].split("\\|")[0];
            int length = Integer.parseInt(speciesSpecification[i].split("\\|")[1]);

            //assign animal to Cage
            if (species.equals("cat")) {
                Cat cat = new Cat(name, length);
                cage[i] = cat.getCage();
            } else if (species.equals("lion")) {
                Lion lion = new Lion(name, length);
                cage[i] = lion.getCage();
            } else if (species.equals("eagle")) {
                Eagle eagle = new Eagle(name, length);
                cage[i] = eagle.getCage();
            } else if (species.equals("parrot")) {
                Parrot parrot = new Parrot(name, length);
                cage[i] = parrot.getCage();
            } else if (species.equals("hamster")) {
                Hamster hamster = new Hamster(name, length);
                cage[i] = hamster.getCage();
            }
        }
        return cage;
    }

    /**
     * treat animal in cage in param.
     * @param animalSpecies species to be treated
     * @param animalCages tobe treated
     */
    public static void treatAnimal(String animalSpecies, Cage[] animalCages) {
        boolean found = false;
        System.out.print("Mention the name of "
                + animalSpecies
                + " you want to visit: ");
        String nameOfAnimal = input.nextLine();
        for (Cage cage : animalCages) {
            if (cage.getAnimal().getName().equals(nameOfAnimal)) {
                System.out.println("You are visiting "
                        + cage.getAnimal().getName()
                        + " (" + animalSpecies
                        + ") now, what would you like to do?");
                System.out.println(cage.getAnimal().treatDescription());
                int choice = input.nextInt();
                input.nextLine();
                if (animalSpecies.equals("parrot")) {
                    input = cage.getAnimal().treatWithScan(choice, input);
                } else {
                    cage.getAnimal().treat(choice);
                }
                found = true;
                break;
            }
        }
        if (!found) {
            System.out.println("There is no "
                    + animalSpecies
                    + " with that name! Back to the office!\n");
        }
    }
}
