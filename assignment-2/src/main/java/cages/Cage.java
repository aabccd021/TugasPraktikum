package cages;

import animals.Animal;

public class Cage {
    Animal animal;
    String location;
    char size;

    public Cage(Animal animal) {
        this.animal = animal;
        if (animal.getType().equals("pet")) {
            this.location = "indoor";
            if (animal.getLength() < 45) {
                this.size = 'A';
            } else if (animal.getLength() < 60) {
                this.size = 'B';
            } else {
                this.size = 'C';
            }
        } else if (animal.getType().equals("wild")) {
            this.location = "outdoor";
            if (animal.getLength() < 75) {
                this.size = 'A';
            } else if (animal.getLength() < 90) {
                this.size = 'B';
            } else {
                this.size = 'C';
            }
        }

    }

    public Animal getAnimal() {
        return this.animal;
    }

    public char getSize() {
        return this.size;
    }

    public String getLocation() {
        return this.location;
    }
}
