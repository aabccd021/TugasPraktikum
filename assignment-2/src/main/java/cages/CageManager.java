package cages;

import java.util.ArrayList;

public class CageManager {

    /**
     * arrange cage in param and return.
     * @param cage tobe arranged
     * @return
     */
    public static ArrayList<ArrayList<Cage>> arrangeCage(Cage[] cage) {
        ArrayList<ArrayList<Cage>> arrangedCage = new ArrayList<ArrayList<Cage>>();
        ArrayList<Cage> one = new ArrayList<Cage>();
        ArrayList<Cage> two = new ArrayList<Cage>();
        ArrayList<Cage> three = new ArrayList<Cage>();
        if (cage.length >= 3) {
            for (int i = 0; i < cage.length / 3; i++) {
                one.add(cage[i]);
            }
            for (int i = cage.length / 3; i < 2 * (cage.length / 3); i++) {
                two.add(cage[i]);
            }
            for (int i = 2 * (cage.length / 3); i < cage.length; i++) {
                three.add(cage[i]);
            }
        } else if (cage.length == 2) {
            two.add(cage[1]);
            one.add(cage[0]);
        } else if (cage.length == 1) {
            one.add(cage[0]);
        }
        arrangedCage.add(one);
        arrangedCage.add(two);
        arrangedCage.add(three);

        return arrangedCage;
    }

    /**
     * print cage in param and return.
     * @param cageArray to be printed
     */
    public static void printArray(ArrayList<ArrayList<Cage>> cageArray) {
        for (int i = 2; i >= 0; i--) {
            int level = i + 1;
            System.out.print("level " + level + ":");
            for (Cage cage : cageArray.get(i)) {
                System.out.print(cage.getAnimal().getName()
                        + " (" + cage.getAnimal().getLength()
                        + " - " + cage.getSize() + "),"
                );
            }
            System.out.println();
        }
        System.out.println();
    }

    /**
     * rearrange cage in param and return.
     * @param cage to be rearranged
     * @return
     */
    public static ArrayList<ArrayList<Cage>> rearrangeCage(ArrayList<ArrayList<Cage>> cage) {
        ArrayList<ArrayList<Cage>> tempRearrangedCage = new ArrayList<ArrayList<Cage>>();
        tempRearrangedCage.add(cage.get(2));
        tempRearrangedCage.add(cage.get(0));
        tempRearrangedCage.add(cage.get(1));
        ArrayList<ArrayList<Cage>> rearrangedCage = new ArrayList<ArrayList<Cage>>();
        rearrangedCage.add(rearrangeLine(tempRearrangedCage.get(0)));
        rearrangedCage.add(rearrangeLine(tempRearrangedCage.get(1)));
        rearrangedCage.add(rearrangeLine(tempRearrangedCage.get(2)));
        return rearrangedCage;
    }

    public static ArrayList<Cage> rearrangeLine(ArrayList<Cage> line) {
        ArrayList<Cage> rearrangedLine = new ArrayList<Cage>();
        for (int i = line.size() - 1; i >= 0; i--) {
            rearrangedLine.add(line.get(i));
        }
        return rearrangedLine;
    }
}
