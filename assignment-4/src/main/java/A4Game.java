import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

public class A4Game {

    private static final String CARD_IMAGE_PATH
            = "C:\\Users\\aabccd021\\IdeaProjects\\TugasPraktikum\\assignment-4\\img\\";
    private static final String cardFileType = ".jpg";
    private static final String CARD_BACK_IMAGE_PATH = CARD_IMAGE_PATH + "back" + cardFileType;

    private static JPanel panel = new JPanel();
    private static ArrayList<JCard> cards = new ArrayList<>();
    private static JCard firstCard = null;
    private static int attempt = 0;
    private static JTextArea attemptsText;

    public static void main(String[] args) {

        for (int i = 1; i <= 18; i++) {
            String frontImagePath = CARD_IMAGE_PATH + "front" + i + cardFileType;
            for (int j = 0; j < 2; j++) {
                System.out.println(i);
                cards.add(new JCard("card" + i, frontImagePath, CARD_BACK_IMAGE_PATH));
            }
        }

        Collections.shuffle(cards);
        for (JCard card : cards) {
            panel.add(card);
        }
        panel.setSize(1440, 720);
        GridLayout layout = new GridLayout(0, 6);
        panel.setLayout(layout);

        JPanel panel2 = new JPanel();

        JButton rePlay = new JButton("Play Again?");
        rePlay.addActionListener(e -> reStart());
        panel2.add(rePlay);

        JButton exit = new JButton("Exit");
        exit.addActionListener(e -> System.exit(0));
        panel2.add(exit);

        attemptsText = new JTextArea("Attempt :" + String.valueOf(attempt));
        panel2.add(attemptsText);

        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.Y_AXIS));
        jPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jPanel.add(panel);
        jPanel.add(panel2);

        JFrame mainFrame = new JFrame();
        mainFrame.add(jPanel);
        mainFrame.setSize(1440, 1000);
        mainFrame.setVisible(true);
    }

    /**
     * Restart the game.
     */
    private static void reStart() {
        Collections.shuffle(cards);
        panel.removeAll();
        for (JCard card : cards) {
            card.close();
            card.setEnabled(true);
            panel.add(card);
        }
        attempt = 0;
        attemptsText.setText(String.valueOf(attempt));
    }

    /**
     * Check status of all cards.
     * When two cards with the same picture opened, both of them become disabled
     * Times of attempt increases every time user opened the 2nd card
     *
     * @param card which just opened
     */
    static void checkCards(JCard card) {
        if (firstCard == null) {
            firstCard = card;
        } else {
            JCard.noCLick = true;
            attempt++;
            attemptsText.setText(String.valueOf(attempt));
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                System.out.println("Interrupted");
                e.printStackTrace();
            }
            if (card.getName().equals(firstCard.getName())) {
                card.setEnabled(false);
                firstCard.setEnabled(false);
            } else {
                card.close();
                firstCard.close();
            }

            firstCard = null;

            JCard.noCLick = false;
        }
    }
}
