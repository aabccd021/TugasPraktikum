import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class JCard extends JButton {
    static boolean noCLick = false;
    private ImageIcon backImage;
    private ImageIcon frontImage;
    private String name;
    private boolean isOpen;

    /**
     * Constructor
     */
    JCard(String name, String cardFrontImagePath, String cardBackImagePath) {
        this.isOpen = false;
        this.name = name;
        //reads image
        try {
            backImage = new ImageIcon(ImageIO.read(new File(cardBackImagePath)).getScaledInstance(240, 135, Image.SCALE_SMOOTH));
            frontImage = new ImageIcon(ImageIO.read(new File(cardFrontImagePath)).getScaledInstance(240, 135, Image.SCALE_SMOOTH));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.setIcon(backImage);

        //Add action when clicked
        this.addActionListener(e -> {
            if (!isOpen && !noCLick) {
                RunnableX runnableX = new RunnableX(this);
                runnableX.start();

                RunnableY runnableY = new RunnableY(this);
                runnableY.start();
            }
        });

        this.setBackground(Color.BLACK);
        this.setSize(200, 100);
    }


    /**
     * Method to check if there is already two cards opened and has same picture.
     */
    void checkCards() {
        A4Game.checkCards(this);
    }

    @Override
    public String getName() {
        return name;
    }

    void close() {
        this.setIcon(backImage);
        this.isOpen = false;
    }

    void open() {
        this.setIcon(frontImage);
        this.isOpen = true;
    }

}

/**
 * Class of threads used to open and close card.
 */
class RunnableX implements Runnable {
    private JCard jCard;
    private Thread thread;

    RunnableX(JCard jCard) {
        this.jCard = jCard;
    }

    @Override
    public void run() {
        jCard.open();
    }

    void start() {
        if (thread == null) {
            thread = new Thread(this, "aaa");
            thread.start();
        }
    }
}

class RunnableY implements Runnable {
    private JCard jCard;
    private Thread thread;

    RunnableY(JCard jCard) {
        this.jCard = jCard;
    }

    void start() {
        if (thread == null) {
            thread = new Thread(this, "bbb");
            thread.start();
        }
    }

    @Override
    public void run() {
        jCard.checkCards();
    }
}

