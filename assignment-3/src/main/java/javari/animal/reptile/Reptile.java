package javari.animal.reptile;

import javari.animal.Animal;
import javari.animal.Condition;
import javari.animal.Gender;

public abstract class Reptile extends Animal {

    private boolean isWild;

    /**
     * Constructs an instance of {@code Animal}.
     *
     * @param id        unique identifier
     * @param type      type of animal, e.g. Hamster, Cat, Lion, Parrot
     * @param name      name of animal, e.g. hamtaro, simba
     * @param gender    gender of animal (male/female)
     * @param length    length of animal in centimeters
     * @param weight    weight of animal in kilograms
     * @param condition health condition of the animal
     */
    public Reptile(Integer id, String type, String name, Gender gender, double length, double weight, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
    }

    @Override
    public void setSpecialStatus(String specialStatus) {
        if (specialStatus.equals("wild")) {
            this.isWild = true;
        }
        if (specialStatus.equals("tame")) {
            this.isWild = false;
        }
    }

    @Override
    protected boolean specificCondition() {
        return !this.isWild;
    }

}
