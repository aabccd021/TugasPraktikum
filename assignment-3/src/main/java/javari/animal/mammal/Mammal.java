package javari.animal.mammal;

import javari.animal.Animal;
import javari.animal.Condition;
import javari.animal.Gender;

public abstract class Mammal extends Animal {
    private boolean isPregnant;

    /**
     * Constructs an instance of {@code Animal}.
     *
     * @param id        unique identifier
     * @param type      type of animal, e.g. Hamster, Cat, Lion, Parrot
     * @param name      name of animal, e.g. hamtaro, simba
     * @param gender    gender of animal (male/female)
     * @param length    length of animal in centimeters
     * @param weight    weight of animal in kilograms
     * @param condition health condition of the animal
     */
    public Mammal(Integer id, String type, String name, Gender gender, double length, double weight, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
    }

    @Override
    public void setSpecialStatus(String specialStatus) {
        if (specialStatus.equals("pregnant")) {
            this.isPregnant = true;
        }
    }

    @Override
    protected boolean specificCondition() {
        return !this.isPregnant;
    }

}
