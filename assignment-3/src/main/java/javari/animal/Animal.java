package javari.animal;

import javari.park.Attraction;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents common attributes and behaviours found in all animals
 * in Javari Park.
 *
 * @author Programming Foundations 2 Teaching Team
 * @author Muhamad Abdurahman
 */
public abstract class Animal {

    private static List<Attraction> attractions = new ArrayList<>();
    private Integer id;
    private String type;
    private String name;
    private Body body;
    private Condition condition;
    private Gender gender;

    /**
     * Constructs an instance of {@code Animal}.
     *
     * @param id        unique identifier
     * @param type      type of animal, e.g. Hamster, Cat, Lion, Parrot
     * @param name      name of animal, e.g. hamtaro, simba
     * @param gender    gender of animal (male/female)
     * @param length    length of animal in centimeters
     * @param weight    weight of animal in kilograms
     * @param condition health condition of the animal
     */
    public Animal(Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.gender = gender;
        this.body = new Body(length, weight, gender);
        this.condition = condition;
    }

    public Animal() {
    }

    /**
     *
     * @param type of Animal which kingdom is going to be searched.
     * @return String kingdom of type
     */
    public static String kingdomOfType(String type) {
        switch (type.toLowerCase()) {
            case "eagle":
                return "Aves";
            case "parrot":
                return "Aves";
            case "cat":
                return "Mammal";
            case "hamster":
                return "Mammal";
            case "lion":
                return "Mammal";
            case "whale":
                return "Mammal";
            case "snake":
                return "Reptile";
            default:
                return null;
        }

    }

    /**
     * Add attraction to attractions list.
     *
     * @param attraction to be added to list
     */
    public static void addAttraction(Attraction attraction) {
        if (!attractions.isEmpty()) {//if attraction is already there
            for (Attraction attraction1 : Animal.attractions) {
                if (attraction1.getName().equals(attraction.getName())
                        && attraction1.getAnimalType().equals(attraction.getAnimalType())) {
                    return;
                }
            }
        }
        Animal.attractions.add(attraction);
    }

    /**
     * get Attraction with specified animal type and attraction name.
     *
     * @param animalType which the attraction to be searched
     * @param attractionName which the attraction to be search
     * @return attractions
     */
    public static Attraction getAttraction(String animalType, String attractionName) {
        if (!attractions.isEmpty()) {
            for (Attraction attraction : attractions) {
                if (attraction.getAnimalType().equalsIgnoreCase(animalType)
                        && attraction.getName().equalsIgnoreCase(attractionName)) {
                    return attraction;
                }
            }
        }
        return null;
    }

    /**
     * get list of attractions of the specified animal type.
     *
     * @param animalType which the list of attractions available will be get
     * @return attractions
     */
    public static List<String> getAttractionsListStringOfAnimal(String animalType) {
        List<String> attractionList = new ArrayList<>();
        for (String attractionName : Attraction.possibleAttractionsName) {
            List<String> animalTypes = Attraction.getAnimalTypeListOfAttraction(attractionName.toLowerCase());
            String lowerAnimalType = animalType.toLowerCase();
            if (animalTypes.contains(lowerAnimalType)) {
                attractionList.add(attractionName);
            }
        }
        return attractionList;

    }

    /**
     * get attraction name by animal type and index specified.
     *
     * @param animalType which the attraction name will be searched
     * @param preferredAnimalNumber
     * @return attraction name
     */
    public static String getAttractionNameByTypeAndIndex(String animalType, int preferredAnimalNumber) {
        String attractionListString = getNumberedAttractionsListStringOfAnimal(animalType);
        String[] lines = attractionListString.split("\\r?\\n");
        for (String line : lines) {
            Integer number = Integer.valueOf(line.split(". ", -1)[0]);
            if (number == preferredAnimalNumber) {
                String a = line.split("\\. ", -1)[1];
                return a;
            }
        }
        return null;
    }

    /**
     * get numbered list of attractions
     *
     * @param animalType which the list will be returned
     * @return numbered list of attractions
     */
    public static String getNumberedAttractionsListStringOfAnimal(String animalType) {
        String attractionList = "";
        Integer attractionCounter = 0;
        for (Attraction attraction : Animal.attractions) {
            if (attraction.getAnimalType().equalsIgnoreCase(animalType)) {
                attractionCounter++;
                attractionList += attractionCounter + ". " + attraction.getName() + "\n";
            }
        }
        return attractionList;
    }

    public static List<Attraction> getAttractions() {
        return attractions;
    }

    public abstract void setSpecialStatus(String specialStatus);

    public String getName() {
        return name;
    }

    /**
     * Returns {@code Gender} identification of the animal.
     *
     * @return
     */
    public Gender getGender() {
        return body.getGender();
    }

    public double getLength() {
        return body.getLength();
    }

    public double getWeight() {
        return body.getWeight();
    }

    /**
     * Returns {@code Condition} of the animal.
     *
     * @return
     */
    public Condition getCondition() {
        return condition;
    }

    /**
     * Determines whether the animal can perform their attraction or not.
     *
     * @return
     */
    public boolean isShowable() {
        return condition == Condition.HEALTHY && specificCondition();
    }

    /**
     * Performs more specific checking to know whether an animal is able
     * to perform or not.
     *
     * @return
     */
    protected abstract boolean specificCondition();

}
