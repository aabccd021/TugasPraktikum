package javari.reader;

import javari.animal.Animal;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class CsvRecordsScanner extends CsvReader {
    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *                     properly
     */
    public CsvRecordsScanner(Path file) throws IOException {
        super(file);
    }

    @Override
    public long countValidRecords() {
        Integer validLineCounter = 0;
        for (String line : this.lines) {
            if (validityOfLine(line) == true) {
                validLineCounter++;
            }
        }
        return validLineCounter;
    }

    @Override
    public long countInvalidRecords() {
        Integer invalidLineCounter = 0;
        for (String line : this.lines) {
            if (validityOfLine(line) == false) {
                invalidLineCounter++;
            }
        }
        return invalidLineCounter;
    }

    @Override
    public boolean validityOfLine(String line) {
        String[] extractedLineData = line.split(",", -1);
        String type = extractedLineData[1];
        String specialStatus = extractedLineData[6];
        if (Animal.kingdomOfType(type).equals("Mammal")) {
            return specialStatus.equals("pregnant") || specialStatus.equals("");
        } else if (Animal.kingdomOfType(type).equals("Aves")) {
            return specialStatus.equals("laying egg") || specialStatus.equals("");
        } else if (Animal.kingdomOfType(type).equals("Reptile")) {
            return specialStatus.equals("tame")
                    || specialStatus.equals("wild")
                    || specialStatus.equals("");
        }
        return false;
    }

    public List<String> getValidLines() {
        List<String> validLines = new ArrayList<>();
        for (String line : lines) {
            if (validityOfLine(line) == true) {
                validLines.add(line);
            }
        }
        return validLines;
    }
}
