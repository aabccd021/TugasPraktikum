package javari.reader;

import javari.animal.Animal;
import javari.park.Attraction;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class CsvAttractionsScanner extends CsvReader {

    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *                     properly
     */
    public CsvAttractionsScanner(Path file) throws IOException {
        super(file);
        generateValidAttractionsList();
    }

    private void generateValidAttractionsList() {
        for (String line : this.lines) {
            if (validityOfLine(line) == true) {
                String[] extractedLineData = line.split(",", -1);
                String animalType = extractedLineData[0];
                String attractionName = extractedLineData[1];
                Animal.addAttraction(new Attraction(attractionName, animalType));
            }
        }
    }

    @Override
    public long countValidRecords() {
        Integer validLineCounter = 0;
        for (String line : this.lines) {
            if (validityOfLine(line) == true) {
                validLineCounter++;
            }
        }
        return validLineCounter;
    }

    @Override
    public long countInvalidRecords() {
        Integer invalidLineCounter = 0;
        for (String line : this.lines) {
            if (validityOfLine(line) == false) {
                invalidLineCounter++;
            }
        }
        return invalidLineCounter;
    }

    public boolean validityOfLine(String line) {
        String[] extractedLineData = line.split(",", -1);
        String animalType = extractedLineData[0];
        String attractionName = extractedLineData[1];
        boolean ccd = Animal.getAttractionsListStringOfAnimal(animalType).contains(attractionName);
        return Animal.getAttractionsListStringOfAnimal(animalType).contains(attractionName);

    }

    public long countValidAttraction() {
        int counter = 0;
        List<String> attractionList = new ArrayList<>();
        for (Attraction attraction : Animal.getAttractions()) {
            if (!attractionList.contains(attraction.getName())) {
                attractionList.add(attraction.getName());
                counter++;
            }
        }
        return counter;
    }

    public long countInvalidAttraction() {
        return this.lines.size() - Animal.getAttractions().size();
    }

}
