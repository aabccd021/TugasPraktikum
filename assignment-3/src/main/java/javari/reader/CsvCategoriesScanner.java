package javari.reader;

import javari.animal.Animal;
import javari.park.Section;

import javax.sound.midi.SoundbankResource;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class CsvCategoriesScanner extends CsvReader {
    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *                     properly
     */
    public CsvCategoriesScanner(Path file) throws IOException {
        super(file);
    }

    public Integer countInvalidSections(List<Section> sections) {
        return 3 - countValidSections(sections);
    }

    public Integer countValidSections(List<Section> sections) {
        int mammal = 1;
        int aves = 1;
        int reptile = 1;
        for (String line : this.lines) {
            String animalType = line.split(",")[0];
            String sectionName = line.split(",")[2];

            boolean ada = false;
            for (Section section : sections) {
                if (section.getAnimalTypesList().contains(animalType)) {
                    ada = true;
                }
            }
            if (!ada) {
                switch (sectionName) {
                    case "Explore the Mammals":
                        mammal = 0;
                        break;
                    case "World of Aves":
                        aves = 0;
                        break;
                    case "Reptillian Kingdom":
                        reptile = 0;
                        break;
                    default:
                        break;
                }
            }
        }
        return mammal + aves + reptile;
    }

    @Override
    public long countValidRecords() {
        int mammal = 1;
        int aves = 1;
        int reptile = 1;
        for (String line : this.lines) {
            String animalType = line.split(",")[0];
            String kingdom = line.split(",")[1];
            boolean benar = false;
            String trueKingdom = "";
            switch (Animal.kingdomOfType(animalType)) {
                case "Mammal":
                    trueKingdom = "mammals";
                    break;
                case "Reptile":
                    trueKingdom = "reptiles";
                    break;
                default:
                    trueKingdom = Animal.kingdomOfType(animalType);
            }
            if (trueKingdom.equalsIgnoreCase(kingdom)) {
                benar = true;
            }

            if (!benar) {
                switch (kingdom) {
                    case "mammals":
                        mammal = 0;
                        break;
                    case "aves":
                        aves = 0;
                        break;
                    case "reptiles":
                        reptile = 0;
                        break;
                    default:
                        break;
                }
            }
        }
        return mammal + aves + reptile;
    }

    @Override
    public long countInvalidRecords() {
        return 3 - countValidRecords();
    }

    @Override
    public boolean validityOfLine(String line) {
        String[] extractedLineData = line.split(",", -1);
        String animalType = extractedLineData[0];
        String animalKingdom = extractedLineData[1];
        return Animal.kingdomOfType(animalType).equalsIgnoreCase(animalKingdom);
    }

}
