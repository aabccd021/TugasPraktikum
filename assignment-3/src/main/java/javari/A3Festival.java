package javari;

import javari.animal.Animal;
import javari.animal.Condition;
import javari.animal.Gender;
import javari.animal.aves.Eagle;
import javari.animal.aves.Parrot;
import javari.animal.mammal.Cat;
import javari.animal.mammal.Hamster;
import javari.animal.mammal.Lion;
import javari.animal.mammal.Whale;
import javari.animal.reptile.Snake;
import javari.park.Attraction;
import javari.park.RegistrationData;
import javari.park.Section;
import javari.park.SelectedAttraction;
import javari.reader.CsvAttractionsScanner;
import javari.reader.CsvCategoriesScanner;
import javari.reader.CsvRecordsScanner;
import javari.writer.RegistrationWriter;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


/**
 * Class to simulate Javari Park Festival Registration Service.
 * Javari Park Consist of 3 Sections. Each section has animals with same kingdom.
 * Each animal has attractions. Animals on certain conditions can not perform on attraction.
 * Visitor can register attraction, and got ticket in JSON format
 * Attractions and animals are provided by CSV on the CsvPath defined.
 */
public class A3Festival {

    private static final String DEFAULT_CSV_PATH
            = "C:\\Users\\aabccd021\\IdeaProjects\\TugasPraktikum\\assignment-3\\data\\";
    private static String CsvPath;
    private static Scanner input;
    private static List<Section> sections = new ArrayList<>();// List of sections in Javari Park
    private static String userName;
    private static RegistrationData userRegistrationData = new RegistrationData();

    /**
     * Main method of the whole registration.
     *
     * @param args just some template
     */
    public static void main(String[] args) {

        //initialization
        CsvPath = DEFAULT_CSV_PATH;
        input = new Scanner(System.in);
        createSections();

        System.out.println("Welcome to Javari Park Festival - Registration Service!");
        System.out.println();
        System.out.print("... Opening default section database from data.");
        try { //open default CsvPath
            readAndPopulateCsvData(DEFAULT_CSV_PATH);
        } catch (IOException e) { //if default CsvPath doesn't have proper csv
            boolean loadSuccess = false;
            while (!loadSuccess) { //keep asking for file CsvPath while Csv files are not found
                System.out.println(" ... File not found or incorrect file!");
                System.out.println();
                System.out.print("Please Provide the source data CsvPath: ");
                try {
                    CsvPath = input.nextLine();
                    readAndPopulateCsvData(CsvPath);
                    loadSuccess = true;
                } catch (IOException e1) { //if file not found
                    loadSuccess = false;
                }
            }
        }

        System.out.println();
        System.out.println("Welcome to Javari Park Festival - Registration Service!");
        System.out.println();
        System.out.println("Please answer the questions by typing the number. "
                + "Type # if you want to return to the previous menu");

        while (true) {
            selectSection();
            System.out.println("Thank you for the interest. "
                    + "Would you like to register to other attractions? (Y/N)");
            String doAttractionAgain = input.nextLine();
            if (doAttractionAgain.equalsIgnoreCase("N")) { //if user input is "N", end the loop
                registerUser(userRegistrationData.getSelectedAttractions());
                return;
            }
        }

    }

    /**
     * Initialize Sections.
     */
    private static void createSections() {
        sections.add(new Section("Explore the Mammals",
                Arrays.asList("Hamster", "Lion", "Cat", "Whale")));
        sections.add(new Section("World of Aves",
                Arrays.asList("Eagle", "Parrot")));
        sections.add(new Section("Reptillian Kingdom",
                Arrays.asList("Snake")));
    }

    /**
     * Read CsvFiles from given directory path and populate valid Csv data's.
     *
     * @param path of CsvFiles
     * @throws IOException if Proper CsvFiles do not exist
     */
    private static void readAndPopulateCsvData(String path) throws IOException {

        //TODO do a verification
        //read files using CsvReader
        CsvCategoriesScanner categoriesScanner
                = new CsvCategoriesScanner(Paths.get(path + "animals_categories.csv"));
        CsvAttractionsScanner attractionsScanner
                = new CsvAttractionsScanner(Paths.get(path + "animals_attractions.csv"));
        CsvRecordsScanner recordsScanner
                = new CsvRecordsScanner(Paths.get(path + "Animals_records.csv"));

        System.out.println("... Loading... Success... System is populating data...");

        //prints out all valid and invalids
        long validSections = categoriesScanner.countValidSections(sections);
        long invalidSections = categoriesScanner.countInvalidSections(sections);
        long numberOfValidAttractions = attractionsScanner.countValidAttraction();
        long numberOfInvalidAttractions = attractionsScanner.countInvalidAttraction();
        long numberOfValidCategories = categoriesScanner.countValidRecords();
        long numberOfInvalidCategories = categoriesScanner.countInvalidRecords();
        long numberOfValidRecords = recordsScanner.countValidRecords();
        long numberOfInvalidRecords = recordsScanner.countInvalidRecords();
        System.out.println("Found " + validSections + " valid sections and "
                + invalidSections + " invalid sections");
        System.out.println("Found " + numberOfValidAttractions + " valid attractions and "
                + numberOfInvalidAttractions + " invalid attractions");
        System.out.println("Found " + numberOfValidCategories + " valid categories and "
                + numberOfInvalidCategories + " invalid categories");
        System.out.println("Found " + numberOfValidRecords + " valid records and "
                + numberOfInvalidRecords + " invalid records");

        List<String> animalsRecords = recordsScanner.getValidLines();

        //populate valid animal data
        populateRecordsData(animalsRecords, Animal.getAttractions());
    }

    /**
     * Populate Data from given animals and attractions.
     *
     * @param animalsRecords data of animals to be instantiated
     * @param attractions    list of attractions available
     */
    private static void populateRecordsData(List<String> animalsRecords,
                                            List<Attraction> attractions) {

        // add each animals to sections
        for (String animalSpecString : animalsRecords) {

            String[] animalSpec = animalSpecString.split(",", -1);
            Integer id = Integer.valueOf(animalSpec[0]);
            String type = animalSpec[1];
            String name = animalSpec[2];
            Gender gender = Gender.parseGender(animalSpec[3]);
            double length = Double.parseDouble(animalSpec[4]);
            double weight = Double.parseDouble(animalSpec[5]);
            String specialStatus = animalSpec[6];
            Condition condition = Condition.parseCondition(animalSpec[7]);
            Animal animal = null;

            switch (type.toLowerCase()) {
                case "eagle":
                    animal = new Eagle(id, type, name, gender, length, weight, condition);
                    break;
                case "parrot":
                    animal = new Parrot(id, type, name, gender, length, weight, condition);
                    break;
                case "cat":
                    animal = new Cat(id, type, name, gender, length, weight, condition);
                    break;
                case "hamster":
                    animal = new Hamster(id, type, name, gender, length, weight, condition);
                    break;
                case "lion":
                    animal = new Lion(id, type, name, gender, length, weight, condition);
                    break;
                case "whale":
                    animal = new Whale(id, type, name, gender, length, weight, condition);
                    break;
                case "snake":
                    animal = new Snake(id, type, name, gender, length, weight, condition);
                    break;
                default:
                    break;
            }

            //add special status if special status exists
            if (specialStatus != null) {
                animal.setSpecialStatus(specialStatus);
            }

            // add animal to the attraction if the kingdom data is right and the animal is show able
            for (Attraction attraction : attractions) {
                if (attraction.getAnimalType().equals(animal.getClass().getSimpleName())
                        && animal.isShowable()) {
                    attraction.addPerformer(animal);
                }
            }
        }
    }

    /**
     * Start Registration Service.
     * User choose which section to register.
     */
    private static void selectSection() {

        //Prints all sections available
        System.out.println();
        System.out.println("Javari Park has 3 sections:");
        String sectionList = "";
        for (int i = 0; i < sections.size(); i++) {
            sectionList += i + 1 + ". " + sections.get(i).getName() + "\n";
        }
        System.out.println(sectionList);

        //User inputs index number of preferred section
        System.out.print("Please choose your preferred section (type the number): ");
        String preferredSectionNumber = input.nextLine();
        if (preferredSectionNumber.equals("#")) { //back to main loop
            return;
        }

        selectAnimal(Integer.parseInt(preferredSectionNumber));
    }

    /**
     * User selects section to be registered.
     *
     * @param sectionNumber number of section choose
     */
    private static void selectAnimal(int sectionNumber) {

        Section section = sections.get(sectionNumber - 1);

        //Prints all animal type available
        System.out.println("--" + section.getName() + "--");
        System.out.println(section.getAnimalTypesList());
        System.out.print("Please choose your preferred animals (type the number): ");

        //User inputs index number og preferred animal
        String choice = input.nextLine();
        if (choice.equals("#")) {
            selectSection();
            return;
        }
        int preferredAnimalNumber = Integer.parseInt(choice);

        selectAttraction(section, preferredAnimalNumber);
    }

    /**
     * User selects attraction to be registered.
     *
     * @param section               section selected
     * @param preferredAnimalNumber index number of animal selected
     */
    private static void selectAttraction(Section section,
                                         int preferredAnimalNumber) {

        int sectionNumber = sections.indexOf(section) + 1;

        String animalType = section.getAnimalTypeString(preferredAnimalNumber);

        //if no animals can perform warn the user
        if (Animal.getNumberedAttractionsListStringOfAnimal(animalType).equals("")) {
            System.out.println("Unfortunately, no " + animalType
                    + " can perform any attraction, please choose other animals");
            selectAnimal(sectionNumber);
            return;
        }

        //Print all available attraction of selected animal and user selects the attraction
        System.out.println("--" + animalType + "--");
        System.out.println(Animal.getNumberedAttractionsListStringOfAnimal(animalType));
        System.out.print("Please choose your preferred attractions (type the number): ");
        String choice = input.nextLine();

        if (choice.equals("#")) { //if user goes back
            selectAnimal(sectionNumber);
            return;
        }

        int preferredAttractionNumber = Integer.parseInt(choice);
        String attractionName = Animal.getAttractionNameByTypeAndIndex(
                animalType,
                preferredAttractionNumber);

        //get attraction from Animal with selected animal and attraction
        Attraction attraction = Animal.getAttraction(animalType, attractionName);

        userRegistrationData.addSelectedAttraction(attraction);
//        userAttraction.add(attraction);
//        registerUser(attraction);
    }

    /**
     * Register user with attraction in parameter.
     *
     * @param attractions to be registered by user
     */
    private static void registerUser(List<SelectedAttraction> attractions) {
        System.out.println("Wow one more step,");

        //User inputs his/her name
        System.out.println("Please let us know your name: ");
        userName = input.nextLine();
        userRegistrationData.setVisitorName(userName);

        //prints attraction and name data
        System.out.println();
        System.out.println("Yeay, final check!");
        System.out.println("here is your data, and the attraction you choose:");
        System.out.println("Name: " + userName);
        System.out.println("Attractions: ");
        for (SelectedAttraction attraction : attractions) {
            System.out.println();
            System.out.println(attraction.getName() + " --> " + attraction.getType());
            System.out.print("With : ");
            for(Animal animal : attraction.getPerformers()) {
                System.out.print(animal.getName() + ", ");
            }
        }
        System.out.println();

        //User confirms the correctioness of data
        System.out.println("Is the data correct? (Y/N): ");
        String dataConfirmation = input.nextLine();

        if (dataConfirmation.equalsIgnoreCase("Y")) { //if registration data is correct
//            RegistrationData userRegistrationData = new RegistrationData(userName, attraction);
            try {
                //write registration data to JSON file
                RegistrationWriter.writeJson(userRegistrationData, Paths.get(CsvPath));
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Failed to write data to JSON File, "
                        + "the directory selected may be invalid");
            }
        }
    }
}
