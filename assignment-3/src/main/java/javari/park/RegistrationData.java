package javari.park;

import java.util.ArrayList;
import java.util.List;

public class RegistrationData implements Registration {

    private static Integer idCounter = 1;
    private Integer registrationId;
    private String visitorName;
    private List<SelectedAttraction> selectedAttractions = new ArrayList<>();

    public RegistrationData() {
        this.registrationId = idCounter;
        idCounter++;
    }

    @Override
    public int getRegistrationId() {
        return this.registrationId;
    }

    @Override
    public String getVisitorName() {
        return this.visitorName;
    }

    @Override
    public String setVisitorName(String name) {
        this.visitorName = name;
        return null;
    }

    @Override
    public List<SelectedAttraction> getSelectedAttractions() {
        return this.selectedAttractions;
    }

    @Override
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        this.selectedAttractions.add(selected);
        return true;
    }
}
