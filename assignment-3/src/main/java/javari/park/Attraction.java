package javari.park;

import javari.animal.Animal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Attraction implements SelectedAttraction {

    public static final List<String> possibleAttractionsName =
            Arrays.asList(new String[]{"Circles of Fires",
                    "Dancing Animals",
                    "Counting Masters",
                    "Passionate Coders"});
    private String name;
    private String animalType;
    private List<Animal> animals = new ArrayList<>();

    public Attraction(String name, String animalType) {
        this.name = name;
        this.animalType = animalType;
    }

    public static List<String> getAnimalTypeListOfAttraction(String attractionName) {
        List<String> animalTypes = new ArrayList<>();
        switch (attractionName.toLowerCase()) {
            case "circles of fires":
                animalTypes.add("lion");
                animalTypes.add("whale");
                animalTypes.add("eagle");
                break;
            case "dancing animals":
                animalTypes.add("parrot");
                animalTypes.add("snake");
                animalTypes.add("cat");
                animalTypes.add("hamster");
                break;
            case "counting masters":
                animalTypes.add("hamster");
                animalTypes.add("whale");
                animalTypes.add("parrot");
                break;
            case "passionate coders":
                animalTypes.add("hamster");
                animalTypes.add("cat");
                animalTypes.add("snake");
                break;
        }
        return animalTypes;
    }

    public String getAnimalType() {
        return animalType;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getType() {
        return this.animalType;
    }

    @Override
    public List<Animal> getPerformers() {
        return this.animals;
    }

    @Override
    public boolean addPerformer(Animal performer) {
        animals.add(performer);
        return true;
    }

}
