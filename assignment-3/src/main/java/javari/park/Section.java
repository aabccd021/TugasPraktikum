package javari.park;

import java.util.List;

public class Section {


    private String name;
    private List<String> animalTypes;

    public Section(String name, List<String> animalTypes) {
        this.name = name;
        this.animalTypes = animalTypes;
    }

    public String getAnimalTypesList() {
        String listOfAnimalNames = "";
        for (int i = 0; i < animalTypes.size(); i++) {
            listOfAnimalNames += i + 1 + ". " + animalTypes.get(i) + "\n";
        }
        return (listOfAnimalNames);
    }

    public String getAnimalTypeString(int index) {
        String a = animalTypes.get(index - 1);
        return a;
    }

    public String getName() {
        return this.name;
    }

}
