public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms

    WildCat cat;
    TrainCar next;

    public TrainCar(WildCat cat) {
        this(cat, null);
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    /**
     * Return total weight of this train and the following trains.
     */
    public double computeTotalWeight() {
        double totalWeight;
        if (next != null) {
            totalWeight = EMPTY_WEIGHT + cat.weight + next.computeTotalWeight();
        } else {
            totalWeight = EMPTY_WEIGHT + cat.weight;
        }
        return totalWeight;
    }

    /**
     * Return total mass index of this train and the following trains.
     */
    public double computeTotalMassIndex() {
        double totalMassIndex;
        if (next != null) {
            totalMassIndex = cat.computeMassIndex() + next.computeTotalMassIndex();
        } else {
            totalMassIndex = cat.computeMassIndex();
        }
        return totalMassIndex;
    }

    /**
     * Print this train and all following trains.
     */
    public void printCar() {
        if (next != null) {
            System.out.print("(" + cat.name + ")--");
            next.printCar();
        } else {
            System.out.print("(" + cat.name + ")");
        }
    }
}
