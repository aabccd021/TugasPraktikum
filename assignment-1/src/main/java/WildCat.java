public class WildCat {

    String name;
    double weight; // In kilograms
    double length; // In centimeters

    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    /**
     * Return massIndex of this instance.
     */
    public double computeMassIndex() {

        double massIndex = (weight * 10000f / (length * length));

        return massIndex;
    }
}
