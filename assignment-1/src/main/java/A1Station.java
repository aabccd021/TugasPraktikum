import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms
    private static int numOfTrainInStation;

    /**
     * Create new Train Car for each user inputs and departs it to Javari Park.
     */
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int inputTrain = Integer.parseInt(input.nextLine());

        numOfTrainInStation = 0;
        TrainCar lastTrain = null;

        for (int trainNumber = 0; trainNumber < inputTrain; trainNumber++) {

            //get input from user
            String[] catSpec = input.nextLine().split(",");
            String catName = catSpec[0];
            Double catWeight = Double.parseDouble(catSpec[1]);
            Double catLength = Double.parseDouble(catSpec[2]);

            //instantiate new train and the cat
            WildCat newCat = new WildCat(catName, catWeight, catLength);
            lastTrain = new TrainCar(newCat,lastTrain);

            numOfTrainInStation++;

            //departs train if the total weight is over the threshold or
            //there is no train to create anymore
            if ((trainNumber == inputTrain - 1)
                || (lastTrain.computeTotalWeight() > THRESHOLD)) {
                depart(lastTrain);
                lastTrain = null;
                numOfTrainInStation = 0;
            }
        }
    }

    /**
     * Return category of mass index in parameter.
     * @param massIndex which whose category will be determined
     */
    public static String massIndexCategory(double massIndex) {
        if (massIndex < 18.5) {
            return "underweight";
        } else if (massIndex < 25) {
            return "normal";
        } else if (massIndex < 30) {
            return "overweight";
        } else {
            return "obese";
        }
    }

    /**
     * Departs train in param.
     * This method prints train which will be departed to Javari Park and it's
     * average mass index.
     * @param departTrain which will be departed
     */
    public static void depart(TrainCar departTrain) {

        System.out.println("The train departs to Javari Park");
        System.out.print("[LOCO]<--");
        departTrain.printCar();

        double avgMassIndex = departTrain.computeTotalMassIndex()
                / (numOfTrainInStation);

        System.out.println("\nAverage mass index of all cats : "
                        + String.format("%.2f", avgMassIndex));
        System.out.println("In average, the cats in the train are *"
                        + massIndexCategory(avgMassIndex) + "*");
    }
}
